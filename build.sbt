// Copyright (c) 2016 Control F1 Ltd
// Made available under the MIT license - see "LICENSE" for full details

import com.typesafe.sbt.packager.docker.Cmd

name := "RSprayCanDocker"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= {
  val akkaV = "2.4.11"
  val jriV = "0.9-6"
  val sprayV = "1.3.4"

  Seq(
    "org.nuiton.thirdparty" % "JRI" % jriV,
    "io.spray" %% "spray-can" % sprayV,
    "io.spray" %% "spray-http" % sprayV,
    "io.spray" %% "spray-routing" % sprayV,
    "com.typesafe.akka" %% "akka-actor" % akkaV
  )
}

enablePlugins(JavaAppPackaging)

packageName in Docker := "controlf1/r-spraycan-docker"
version in Docker := "latest"

dockerBaseImage := "controlf1/openjre-with-r:8_3.3"

dockerExposedPorts += 8080

// Allow JRI to be able to find the native bridge to R
javaOptions in Universal += "-Djava.library.path=/opt/lib"

// Allow the bridge to be able to find R itself
dockerCommands += Cmd("ENV R_HOME=/usr/lib/R")

