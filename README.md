# R, spray-can and Docker

A simple example of how to use spray-can (from [Spray](http://spray.io/)) and
[Docker](https://www.docker.com/) to wrap an [R](https://www.r-project.org/)
calculation into a micro-service accessible over HTTP.

For more details, see [our blog post](https://controlf1.wordpress.com/2016/10/31/r-spray-can-and-docker/).