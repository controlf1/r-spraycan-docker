// Copyright (c) 2016 Control F1 Ltd
// Made available under the MIT license - see "LICENSE" for full details

package uk.co.controlf1.rspraycandocker

import akka.actor.Actor
import spray.routing.HttpService

/**
  * The actor which drives our HTTP service
  */
class RSprayCanDockerActor extends Actor with HttpService {
  private def route =
    path("add" / DoubleNumber / DoubleNumber) { (a, b) =>
      get {
        complete {
          // Here is where we do the actual R calculation. In the real world,
          // you'd probably be doing something more complicated than adding two
          // numbers!
          val trySum = SynchronizedRengine.performRCalculation { re =>
            // Make our variables visible to R
            re.assign("v", Array(a, b))

            // And get the result back from R
            re.eval("sum(v)")
          }

          // Extract the result from the REXP returned from the JRI
          trySum map { _.asDoubleArray.head.toString() }
        }
      }
    }

  def actorRefFactory = context
  def receive = runRoute(route)
}
