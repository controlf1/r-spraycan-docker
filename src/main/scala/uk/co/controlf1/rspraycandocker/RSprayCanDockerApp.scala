// Copyright (c) 2016 Control F1 Ltd
// Made available under the MIT license - see "LICENSE" for full details

package uk.co.controlf1.rspraycandocker

import scala.concurrent.duration._

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http

/**
  * Main entry point
  */
object RSprayCanDockerApp extends App {
  implicit val system = ActorSystem("RSprayCanDocker")
  val service = system.actorOf(Props[RSprayCanDockerActor], "RSprayCanDocker")
  implicit val timeout = 5.seconds

  // It's important we bind to 0.0.0.0 here rather than localhost as the
  // requests will be from an external source when running in Docker
  IO(Http) ! Http.Bind(service, interface = "0.0.0.0", port = 8080)
}
