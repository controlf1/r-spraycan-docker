// Copyright (c) 2016 Control F1 Ltd
// Made available under the MIT license - see "LICENSE" for full details

package uk.co.controlf1.rspraycandocker

import org.rosuda.JRI.{RMainLoopCallbacks, Rengine}

/**
  * A set of R main loop callbacks which mostly do nothing
  */
class StubRMainLoopCallbacks extends RMainLoopCallbacks {
  def rBusy(re: Rengine, which: Int) {
  }

  def rChooseFile(re: Rengine, newFile: Int) = {
    null
  }

  def rFlushConsole(re: Rengine) {
  }

  def rLoadHistory(re: Rengine, filename: String) {
  }

  def rReadConsole(re: Rengine, prompt: String, addToHistory: Int) = {
    null
  }

  def rSaveHistory(re: Rengine, filename: String) {
  }

  def rShowMessage(re: Rengine, message: String): Unit = {
  }

  /**
    * The R engine requested a message to be displayed. This implementation
    * stores any error in the singleton's storage area
    *
    * @param re The R engine
    * @param text The text of the message
    * @param oType The type of the message
    */
  def rWriteConsole(re: Rengine, text: String, oType: Int) {
    oType match {
      case 0 => // Informational message, do nothing
      case _ => SynchronizedRengine.setRException(RException(text))
    }
  }
}

/**
  * Companion object
  */
object StubRMainLoopCallbacks {
  /**
    * Scala-style object creation
    *
    * @return A new StubRMainLoopCallbacks instance
    */
  def apply() = new StubRMainLoopCallbacks()
}
