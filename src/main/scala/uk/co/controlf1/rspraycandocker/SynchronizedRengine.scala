// Copyright (c) 2016 Control F1 Ltd
// Made available under the MIT license - see "LICENSE" for full details

package uk.co.controlf1.rspraycandocker

import scala.util.{Failure, Success, Try}

import org.rosuda.JRI.Rengine

/**
  * Wrapper for Rengine to ensure exceptions are captured
  *
  * @param re The underlying R engine
  */
class SynchronizedRengine(val re: Rengine) {

  // Proxy through to the underlying Rengine assign functions. You'll probably
  // want to add some more of these in a real world implementation.
  def assign(symbol: String, value: Array[Double]) = re.assign(symbol, value)

  /**
    * Evaluate an R expression, capturing any errors in a Try
    *
    * @param expression The R expression to be evaluated
    * @return The result of evaluating the expression
    */
  def eval(expression: String) = {
    SynchronizedRengine.rException = None
    val maybeResult = Option(re.eval(expression))
    maybeResult match {
      case Some(result) => Success(result)
      case None => Failure(SynchronizedRengine.rException.getOrElse(RException("Undefined R error")))
    }
  }
}

/**
  * Wrapper to ensure that only one thread can access the singleton R engine at once
  */
object SynchronizedRengine {
  /**
    * Dummy object to be used to synchronize
    */
  private val lock = new Object()

  /**
    * The exception representing the error from the R engine
    */
  private var rException: Option[Throwable] = None

  /**
    * Perform an R calculation
    *
    * @param fn Callback which will actually perform the calculation. The callback should be single-threaded.
    * @tparam T The type of the calculation result
    * @return The result of the calculation
    */
  def performRCalculation[T](fn: SynchronizedRengine => Try[T]) = {
    lock.synchronized {
      val noSaveArgs = Array("--no-save")
      val re = Option(Rengine.getMainEngine).getOrElse(new Rengine(noSaveArgs, false, StubRMainLoopCallbacks()))
      val synchronizedRe = SynchronizedRengine(re)
      try {
        fn(synchronizedRe)
      } finally re.end()
    }
  }

  /**
    * Set the R engine exception
    *
    * @param exception The exception which occurred
    */
  def setRException(exception: Throwable) = rException = Option(exception)

  /**
    * Scala-style object creation
    *
    * @param re The underlying R engine to be wrapped
    * @return The synchronized R engine object
    */
  def apply(re: Rengine) = new SynchronizedRengine(re)
}
