// Copyright (c) 2016 Control F1 Ltd
// Made available under the MIT license - see "LICENSE" for full details

package uk.co.controlf1.rspraycandocker

/**
  * An exception representing an error from R
  *
  * @param message The message from R
  */
case class RException(message: String) extends Exception {
}
